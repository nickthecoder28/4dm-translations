{
    "LanguageName": "Română",

    "LoadingText": "Se încarcă...",
    "OkButton": "OK",
    "CancelButton": "Anulează",
    "YesButton": "Da",
    "NoButton": "Nu",
    "ContinueButton": "Continuă",
    "BackButton": "Înapoi",
    "ExitButton": "Ieși",
    "DefaultValueText": "{} (implicit)",
    "ErrorPopup": {
        "TitleText": "Eroare!"
    },
    
    "TitleScreen": {
        "DemoText": "DEMO",
        "SingleplayerButton": "Singleplayer",
        "MultiplayerButton": "Multiplayer",
        "TutorialButton": "Tutorial",
        "SettingsButton": "Setări",
        "CreditsButton": "Contribuiri",
        "QuitButton": "Ieși din joc"
    },
    
    "Singleplayer": {
        "YourWorldsText": "Lumile tale:",
        "NoWorldsText": "Nu este nicio lume de încărcat...",
        "InvalidWorldButton": "{} (invalidă)",
        "OldWorldButton": "{} (veche)",
        "NewWorldButton": "Lume nouă",
        "WorldConversionPopup": {
            "TitleText": "Dorești să convertești lumea la formatul nou?",
            "OldWorldVersionText": "Lumea \"{}\" nu poate fi încărcată deoarece folosește o versiune veche a formatului lumii ({}), incompatibilă cu versiunea nouă ({}).",
            "OldWorldVersionlessText": "Lumea \"{}\" nu poate fi încărcată deoarece folosește o versiune veche a formatului lumii, incompatibilă cu versiunea nouă.",
            "ConversionPromptText": "Dorești să convertești lumea la versiunea actuală? Vei avea opțiunea de a face o copie mai întâi."
        },
        "InvalidWorldPopup": {
            "TitleText": "Lume invalidă!",
            "InvalidVersionText": "Lumea \"{}\" nu poate fi încărcată deoarece este dintr-o versiune viitoare a 4D Miner sau are un număr de versiune invalid."
        }
    },
    
    "CreateWorld": {
        "WorldNameText": "Numele lumii:",
        "WorldNameInput": "Lume nouă",
        "WorldSeedText": "Sămânța lumii:",
        "FlatWorldCheckbox": "Lume plată",
        "GenerateCavesCheckbox": "Generează peșteri (încet)",
        "CaveSizeSlider": "Mărimea peșterilor: {}%",
        "CaveFrequencySlider": "Frecvența peșterilor: {}%",
        "CaveDetailLevelSlider": "Nivelul de detaliu al peșterilor: {} (afectează grav performanța)",
        "BiomeSizeSlider": "Mărimea biomurilor: {}%",
        "TerrainAmplificationSlider": "Amplificarea terenului: {}%",
        "CreateNewWorldButton": "Creează o nouă lume!",
        "NoNamePopup": {
            "NoNameText": "Nu poți crea o lume făra nume!"
        }
    },

    "WorldConverter": {
        "CloseWarningText": "NU închide jocul!",
        "BackupWorldTitle": "{} (copie de rezervă)",
        "BackupStatusText": "Se creeaza o copie de rezervă a lumii \"{}\"...",
        "ConversionStatusText": "Se convertește lumea \"{}\" de la versiunea {} la versiunea {}...",
        "ConversionFinishedText": "Terminat!",
        "BackupPopup": {
            "TitleText": "Dorești să creezi o copie de rezervă a lumii \"{}\"?",
            "PromptText": "Datele lumii \"{}\" s-ar putea corupe în timpul convertirii. Dorești să faci o copie mai întâi?"
        },
        "ErrorPopup": {
            "GenericConversionErrorText": "A apărut o eroare în timpul actualizării lumii la versiunea {}.",
            "InvalidObjectText": "Nu se poate copia obiectul invalid \"{}\" (era prevăzut un dosar sau fișier normal).",
            "CopyErrorText": "Nu se poate copia obiectul: \"{}\": \"{}\"",
            "MissingChunksDirText": "Lipsește dosarul \"chunks\".",
            "UnableToOpenFileText": "Nu se poate deschide fișierul \"{}\"."
        },
        "ConversionStatus": {
            "CleaningUpText": "Curățare..."
        }
    },
    
    "WorldGen": {
        "ProgressText": "{}/{} chunkuri încărcate"
    },

    "Settings": {
        "ControlsButton": "Controale",
        "FullscreenButton": {
            "EnterText": "Activează ecranul complet",
            "ExitText": "Dezactivează ecranul complet"
        },
        "RenderDistanceSlider": "Distanță vizuală: {}",
        "ScrollSensitivitySlider": "Sensibilitatea scroll-ului:",
        "LookSensitivitySlider": "Sensibilitatea privirii:",
        "InvertLookXCheckbox": "Invertește axa X a mouse-ului",
        "InvertLookYCheckbox": "Invertește axa Y a mouse-ului",
        "FovSlider": {
            "LabelText": "Câmp vizual: {}",
            "ValueText": "{} grade"
        },
        "DifficultySlider": {
            "LabelText": "Dificultate: {}",
            "EasyText": "Ușor (fără monștri)",
            "NormalText": "Normal",
            "HardText": "Greu"
        },
        "SmoothLightingCheckbox": "Lumină fină",
        "ShadowsCheckbox": "Umbre",
        "ColoredLightsCheckbox": "Lumini colorate",
        "Forg": {
            "LabelText": "Broscă: {}",
            "Names": {
                "Frank": "Frank",
                "Fernanda": "Fernanda"
            }
        },
        "Audio": {
            "LabelText": "Audio:",
            "GlobalVolumeSlider": "Volum global:",
            "MusicVolumeSlider": "Volumul muzicii:",
            "CreatureVolumeSlider": "Volumul ființelor:",
            "AmbienceVolumeSlider": "Volumul sunetelor ambientale:"
        },
        "Multiplayer": {
            "LabelText": "Multiplayer:",
            "ChatCheckbox": "Afișează chatul",
            "NametagsCheckbox": "Afișează numele jucătorilor",
            "SkinsCheckbox": "Afișează skinurile personalizate"
        }
    },

    "Tutorial": {
        "Slideshow": {
            "Slide1": {
                "Text1": "Acest tutorial este făcut pentru a te ajuta să înțelegi spațiul cvadridimensional.\n\n\n\nComparând diferențele dintre un spațiu 2D și unul 3D, vei putea intui cum se comportă spațiul 3D în raport cu cel 4D."
            },
            "Slide2": {
                "Text1": "Acesta este {}, o broscă (da, o BROSCĂ)",
                "Text2": "Broștele sunt ființe 2D, adică nu pot exista decât într-un spațiu 2D, dar asta nu înseamnă neapărat că nu pot explora a treia dimensiune!\n\n\n\nBroștele se pot deplasa în interiorul unei secțiuni plane a spațiului 3D care le înconjoară."
            },
            "Slide3": {
                "Text1": "De asemenea, broștele au abilitatea specială de a-și roti planul de existență în a treia dimensiune, ceea ce le permite să vadă orice secțiune plană vor.",
                "Text2": "În următoarea parte vei putea să îl controlezi pe {} și să experimentezi chiar tu această abilitate!"
            },
            "Slide4": {
                "Text1": "Aceeași logică este valabilă pentru a treia și a patra dimensiune.\n\n\nPoți scrola din mouse pentru a-ți roti vederea lumii 4D, văzând porțiuni cu totul noi ale terenului!",
                "Text2": "Acum știi să joci 4D Miner! Dacă vrei ajutor în a naviga a patra dimensiune, caută",
                "Text3": "busola!"
            }
        },
        "ForgGame": {
            "2DViewText": "WASD sau săgeți pentru a deplasa brosca {}.\n\nScrolează din mouse pentru a roti secțiunea plană!",
            "3DViewText": "Click + trage pentru a potrivi vederea 3D"
        }
    },

    "Game": {
        "Chat": {
            "LabelText": "Chat:"
        },
        "CraftingMenu": {
            "LabelText": "Fabricare:"
        },
        "Compass": {
            "PositionText": "Poziție: {0}, {1}, {2}, {3}",
            "FacingDirectionText": "Privești în direcția: {0}, {1}, {2}, {3}",
            "XText": "X: {}",
            "YText": "Y: {}",
            "ZText": "Z: {}",
            "WText": "W: {}"
        },
        "Chest": {
            "LabelText": "Cufăr:"
        }
    },

    "Pause": {
        "PausedText": "Pauză...",
        "BackToGameButton": "Înapoi la joc...",
        "QuitToTitleButton": "Salvează și ieși"
    },

    "Items": {
        "Air": "Aer",
        "Grass": "Iarbă",
        "Dirt": "Pământ",
        "Stone": "Rocă",
        "Wood": "Lemn",
        "Leaf": "Frunză",
        "SemiMoltenRock": "Roca semitopită",
        "IronOre": "Minereu de fier",
        "DeadlyOre": "Minereu mortal",
        "Chest": "Cufăr",
        "MidnightGrass": "Iarbă de miazănoapte",
        "MidnightSoil": "Sol de miazănoapte",
        "MidnightStone": "Rocă de miazănoapte",
        "MidnightWood": "Lemn de miazănoapte",
        "MidnightLeaf": "Frunză de miazănoapte",
        "Bush": "Tufiș",
        "MidnightBush": "Tufiș de miazănoapte",
        "RedFlower": "Floare roșie",
        "WhiteFlower": "Floare albă",
        "BlueFlower": "Floare albastră",
        "TallGrass": "Iarbă înaltă",
        "Sand": "Nisip",
        "Sandstone": "Gresie",
        "Cactus": "Cactus",
        "Snow": "Zăpadă",
        "Ice": "Gheață",
        "SnowyBush": "Tufiș înzăpezit",
        "Glass": "Sticlă",
        "SolenoidOre": "Minereu de solenoid",
        "SnowyLeaf": "Frunză înzăpezită",
        "Pumpkin": "Dovleac",
        "JackOLantern": "Dovleac de Halloween",
        "PalmWood": "Lemn de palmier",
        "PalmLeaf": "Frunză de palmier",
        "Water": "Apă",
        "Lava": "Lavă",
        "GrassSlab": "Lespede de iarbă",
        "DirtSlab": "Lespede de pământ",
        "StoneSlab": "Lespede de piatră",
        "WoodSlab": "Lespede de lemn",
        "MidnightStoneSlab": "Lespede de piatră de miazănoapte",
        "MidnightWoodSlab": "Lespede de lemn de miazănoapte",
        "SandstoneSlab": "Lespede de gresie",
        "SnowSlab": "Lespede de zăpadă",
        "IceSlab": "Lespede de gheață",
        "GlassSlab": "Lespede de iarbă",
        "PalmWoodSlab": "Lespede de lemn de palmier",
        "Stick": "Băț",
        "Hammer": "Ciocan",
        "IronPick": "Târnăcop de fier",
        "DeadlyPick": "Târnăcop mortal",
        "IronAxe": "Topor de fier",
        "DeadlyAxe": "Topor mortal",
        "Ultrahammer": "Ultraciocan",
        "SolenoidCollector": "Colector de solenoid",
        "Rock": "Piatră",
        "Hypersilk": "Hiperpânză",
        "IronBars": "Lingouri de fier",
        "DeadlyBars": "Lingouri mortale",
        "SolenoidBars": "Lingouri de solenoid",
        "Compass": "Busolă",
        "4DGlasses": "Ochelari 4D",
        "KleinBottle": "Sticla lui Klein",
        "HealthPotion": "Poțiune de viață",
        "RedLens": "Lentilă roșie",
        "GreenLens": "Lentilă verde",
        "BlueLens": "Lentilă albastră",
        "Alidade": "Alidadă",
        "Sign": "Semn",
        "Bucket": "Găleată",
        "WaterBucket": "Găleată cu apă",
        "LavaBucket": "Găleată cu lavă"
    }
}